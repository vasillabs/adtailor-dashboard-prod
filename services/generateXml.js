const builder = require('xmlbuilder')

var DataPool = {
    name: ["Operating System", 'Ethnicity', 'Age', 'Screen sizes', 'Country'],
    values: {
        "Operating System": ['iOS', 'JVM', 'Android', 'Windows', 'OS X', 'macOS', 'Chrome OS', 'Linux'],
        "Ethnicity": ['Hispanic or Latino', 'White', 'Indian', 'Asian', 'Native American'],
        "Age": ['Average Age', 'Young', 'Senior', 'Older'],
        "Screen sizes": ['768x1024', '0x0', '375x667', '360x640', '1600x900'],
        "Country": ['United States', 'Canada', 'United Kingdom']
    }
}

var dataRandomizer = (function () {
    function numbers(max) {
        return Math.floor(Math.random() * max)
    }

    function generateCriteriaData() {
        var criteriaObj = []

        for (var i = 0; i < 4; i++) {
            criteriaObj.push({
                name: DataPool.name[i],
                values: DataPool.values[DataPool.name[i]]
            })
        }

        if (criteriaObj !== null) {
            return criteriaObj
        }
    }

    return {
        numbers: numbers,
        generateCriteriaData: generateCriteriaData
    }
})()

class XMLGenerator {
    constructor() {
        this.data = dataRandomizer.generateCriteriaData()
        this.root = this.buildRoot()
    }

    buildAttributes(newElement, attributes) {
        attributes.forEach(attribute => {
            console.log("Log attribute: " + attribute);
        })
    }

    buildRoot() {
        return builder.create('response')
            .att('quota', "10000")
            .att('success', "true")
            .att('used', `${dataRandomizer.numbers(5000)}`)
            .att('usedproc', `${dataRandomizer.numbers(100)}`)
            .att('visits', `${dataRandomizer.numbers(10000)}`)
    }

    generateCriteriaElement() {
        var results;
        this.data.forEach(obj => {
            results = this.root.ele('results')
                .att('criteria', `${obj.name}`)
                .att('visits', `${dataRandomizer.numbers(10000)}`)

            obj.values.forEach(value => {
                results.ele('result', {
                    'count': `${dataRandomizer.numbers(15000)}`,
                    'proc': `${dataRandomizer.numbers(100)}`,
                    'value': `${value}`
                })
            })
        })
    }

    generateStatesElement() {
        var states = this.root.ele('states')
        for (let i = 0; i < 84; i++) {
            states.ele('state', {
                    'scale': `${dataRandomizer.numbers(10)}`,
                    'visits': `${dataRandomizer.numbers(15000)}`
                })
                .up()
        }
    }

    stringifyRoot() {
        return this.root.toString({
            pretty: true
        })
    }

    init() {
        this.generateCriteriaElement()
        this.generateStatesElement()
        var generatedDocument = this.stringifyRoot()
        return generatedDocument
    }

    build() {
        return this.init()
    }
}

module.exports = XMLGenerator