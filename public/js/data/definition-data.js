var criterias = [{
    name: 'ethnicity',
    description: 'The ethnic background of the user'
  },
  {
    name: 'age',
    description: 'The age group the person fits in (from young to senior)'
  },
  {
    name: 'education',
    description: 'The educational level of the user '
  },
  {
    name: 'householdIncome',
    description: 'The household income compared to average rates'
  },
  {
    name: 'householdIncomeLocal',
    description: 'The localized household income, compared to state or provincial averages'
  },
  {
    name: 'healthInsurance',
    description: 'The probability of health insurance coverage of the user'
  },
  {
    name: 'povertyLevel',
    description: 'The risk of poverty of the user'
  },
  {
    name: 'crashDeath',
    description: 'The motor vehicle crash deaths rates in the user’s location compared with the national averages'
  },
  {
    name: 'diabetes',
    description: 'Prevalence of diabetes compared with the national averages'
  },
  {
    name: 'diabeticScreening',
    description: 'Likelihood to have undergone screening for diabetes'
  },
  {
    name: 'excessiveDrinking',
    description: 'Probability of abusive alcohol consummation habits of the user'
  },
  {
    name: 'fastFood',
    description: 'Availability of and access to fast food restaurants according to the user’s location'
  },
  {
    name: 'fineParticulateMatter',
    description: 'Air pollution - fine particulate matter according to the user’s location'
  },
  {
    name: 'mammographyScreening',
    description: 'Likelihood to have undergone mammography screening'
  },
  {
    name: 'obesity',
    description: 'Probability of obesity of the user compared with the average values'
  },
  {
    name: 'physicalInactivity',
    description: 'Probability of physical inactiveness of the user'
  },
  {
    name: 'poorHealth',
    description: 'Poor or fair health rates compared with the average values'
  },
  {
    name: 'prematureDeath',
    description: 'Risk of premature death compared with the average values'
  },
  {
    name: 'recreationalFacilities',
    description: 'Access to recreational facilities compared with the average values'
  },
  {
    name: 'sexTransDis',
    description: 'Probability of user with sexual transmitted diseases compared with the average values'
  },
  {
    name: 'smoking',
    description: 'Smoking habits compared with the average values'
  },
  {
    name: 'teenBirths',
    description: 'Teen births rates compared with the average values'
  },
  {
    name: 'crime',
    description: 'Probability of being a victim of a violent crime'
  },
  {
    name: 'unemployment',
    description: 'Unemployment rates compared with the average values'
  },
  {
    name: 'mortgage',
    description: 'Likelihood of having a mortgage'
  },
  {
    name: 'election',
    description: 'Voting Preferences in the United States – democratic or republican candidate (US only)'
  },
  {
    name: 'creditRate',
    description: 'Credit score rates'
  }
]