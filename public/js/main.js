// :: @ criterias -- GLOBAL variable comes from criteria data document

const sumArrayElements = (function () {
	Array.prototype.sum = function (prop) {
		let total = 0
		for (let i = 0, _len = this.length; i < _len; i++) {
			total += this[i][prop]
		}
		return Number(total)
	}
})();

const utils = {
	randomElementFromArray: (array) => {
		const rand = Math.floor(Math.random() * array.length);
		return array[rand];
	},
	randomNumber: (max) => {
		return Math.floor(Math.random() * max);
	},
	randomArrayOfNumbers: (len) => {
		let arrayOfNums = [];
		for (let i = 0; i < len; i++) {
			var arrayElement = utils.randomNumber(100);
			arrayOfNums.push(arrayElement)
		}

		return arrayOfNums;
	}
}

const criteriaBar = function () {
	const DOM = {
		select: $('#propertyid'),
		bar: $('.criteria-bar'),
		name: $('.criteria-bar__name'),
		definition: $('.criteria-bar__definition')
	}

	function showBox() {
		DOM.bar.alert('show').fadeIn().addClass('fade show')
	}

	function hideBox() {
		DOM.bar.fadeOut()
	}

	function handleSelect() {
		showBox()
		const selectedOption = this.options[this.selectedIndex].innerHTML
		const findInCriteria = criterias.find(criteria => {
			return criteria.name == selectedOption
		})
		if (!findInCriteria) return DOM.bar.fadeOut()

		const criteria = {
			name: findInCriteria.name,
			description: findInCriteria.description
		}
		updateBox(criteria)
	}

	function updateBox(criteria) {
		DOM.name.html(criteria.name)
		DOM.definition.html(criteria.definition)
	}

	DOM.select.on('change', handleSelect)
}

const criteriaSearchWidget = function () {
	const DOM = {
		wrapper: $('.criteria-search'),
		input: $('.criteria-search__input'),
		box: $('.criteria-search__box'),
		content: $('.criteria-search__content'),
		title: $('.criteria-search__title'),
		description: $('.criteria-search__description')
	}

	function handleSearchInput(event) {
		clearBox()

		let inputValue = $(event.target).val()
		if (inputValue.length === 0) return DOM.box.hide()
		if (inputValue.length > 0) DOM.box.show()
		cleanedInput = inputValue.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
		var queryRegExp = new RegExp('^' + cleanedInput, 'i');

		// @ criterias -- GLOBAL variable comes from criteria data document
		var results = criterias.filter(function (criteria) {
			return queryRegExp.test(criteria.name);
		});

		updateBox(results)
	}

	function clearBox() {
		DOM.content.empty()
	}

	function updateBox(results) {
		results.forEach(function (criteria) {
			DOM.content.append(`<h5 class="card-title criteria-search__title text-primary">${criteria.name}</h5>`)
			DOM.content.append(`<p class="card-text criteria-search__description">${criteria.description}</p>`)
		})
	}

	DOM.input.on('keyup', function (e) {
		handleSearchInput(e)
	})
}

const spinner = (function () {
	return {
		show: show,
		hide: hide
	}

	function _build() {
		$('body').append(`
			<div class="spinner">
				<div class="lds-grid">
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
					<div></div>
				</div>
		 	</div>`)
	}

	function show() {
		_build()
		$('.spinner').addClass("active");
	}

	function hide() {
		$('.spinner').removeClass("active")
	}
})();

const navModule = function () {
	const DOM = {
		toggler: $('.main-nav-toggle'),
		sidebar: $('.page-sidebar'),
		nav: $('.side-menu'),
		navItems: $('.side-menu').find('li'),
		dropdown: $('.nav-2-level')
	}

	$.each(DOM.navItems, (index, item) => {
		$(item).on("click", function (e) {
			e.preventDefault()
			DOM.dropdown.toggle('collapse')
		})
	})

	DOM.toggler.on("click", function (e) {
		DOM.sidebar.toggle('opened');
	})
};

const EasyPie = {
	init: function (pies) {
		if (pies === undefined || pies === null) return false;

		let self = this;
		const colors = ['#5a68bf', '#18c5a9', '#3499e2'];

		const DOMelements = [
			'.easy-value-1',
			'.easy-value-2',
			'.easy-value-3'
		];

		pies.map((pie, index) => {
			const selectedColor = colors[index];
			const data = {
				class: pie.class,
				options: {
					barColor: selectedColor,
					scaleColor: false,
					lineWidth: 6,
					size: 60,
					lineCap: 'circle'
				}
			}
			const currentPie = $(data.class).easyPieChart(data.options)
		})
	},
	update: function (pies) {
		pies.map((pie, index) => {
			const pieClass = pie.class;
			const piePercentage = pie.percentage;
			$(pieClass).data('easyPieChart').update(piePercentage);
		})
	},
	updateDomValues: function (data) {
		const userCriteriaName = $(".user-info-box__criteria");
		const userCriteriaValue = $(".user-info-box__text");

		for (let i = 0; i < userCriteriaName.length; i++) {
			userCriteriaName[i].innerHTML = data[0].criteriaName[i].name
		}

		for (let i = 0; i < userCriteriaValue.length; i++) {
			userCriteriaValue[i].innerHTML = data[0].criteriaValue
		}

		const elm1 = $('.easy-value-1')
		const elm2 = $('.easy-value-2')
		const elm3 = $('.easy-value-3')

		elm1.html(data[0].visits)
		elm2.html(`${data[1].percentage} %`)
		elm3.html(`${data[2].percentage} %`)
	}
}
const Doughnut = {
	init: function (ctx, dataValues) {
		if (ctx === undefined || dataValues === undefined) return false;

		const itemText1 = $('.device-box__value--1')
		const itemText2 = $('.device-box__value--2')
		const itemText3 = $('.device-box__value--3')

		itemText1.find('span').html(utils.randomNumber())
		itemText2.find('span').html(utils.randomNumber())
		itemText3.find('span').html(utils.randomNumber())

		const donutData = {
			datasets: [{
				data: dataValues,
				backgroundColor: ["rgb(90, 104, 191)", "rgb(24, 197, 169)", "rgb(52, 153, 226)"],
				borderWidth: 2
			}]
		}

		return new Chart(ctx, {
			type: 'doughnut',
			data: donutData
		});
	},
	update: function (chart, data) {
		if (chart === undefined || data === undefined) return false;

		chart.data.datasets.forEach((dataset) => {
			dataset.data = data;
		});
		chart.update();
	}
}
const BarChart = {
	init: function (ctx, lineData, labelsArray) {
		if (ctx === undefined || lineData === undefined || labelsArray === undefined) return false;

		var gradientStroke = ctx.createLinearGradient(500, 0, 100, 0);
		var gradientFill = ctx.createLinearGradient(500, 0, 100, 0);
		gradientStroke.addColorStop(0, '#3499e2');
		gradientStroke.addColorStop(1, '#18c5a9');
		gradientFill.addColorStop(0, '#3499e2');
		gradientFill.addColorStop(1, '#18c5a9');

		var lineOptions = {
			responsive: true,
			maintainAspectRatio: false,
			showScale: false,
			scales: {
				xAxes: [{
					gridLines: {
						display: false,
						drawBorder: false,
					},
					barPercentage: 0.8,
					categoryPercentage: 0.8
				}],
				yAxes: [{
					display: false,
					gridLines: false,
					gridLines: {
						display: false,
						drawBorder: false,
					},
				}]
			},
			legend: {
				display: false
			}
		};

		var lineData = {
			labels: labelsArray,
			datasets: [{
				label: "Google Chrome",
				backgroundColor: gradientFill,
				hoverBackgroundColor: gradientFill,
				data: lineData
			}]
		};

		return new Chart(ctx, {
			type: 'bar',
			data: lineData,
			options: lineOptions
		});
	},
	update: function (chart, data) {
		if (chart === undefined || data === undefined) return false;

		chart.data.datasets.forEach((dataset) => {
			dataset.data = data;
		});
		chart.update();
	}
}
var DefaultWidget = {
	init: function () {
		$('.easypie').each(function () {
			$(this).easyPieChart({
				trackColor: $(this).attr('data-trackColor') || '#f2f2f2',
				scaleColor: false
			});
		});
	}
}

var DefaultWidget1 = {
	init: function () {
		var ctx = document.getElementById("visitors_chart").getContext("2d");
		var visitors_chart = new Chart(ctx, {
			type: 'line',
			data: {
				labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "July"],
				datasets: [{
					label: "USA",
					data: [20, 18, 40, 10, 35, 24, 40],
					borderColor: 'rgba(90, 104, 191,1)',
					backgroundColor: 'rgba(90, 104, 191,.4)',
					borderWidth: 1
				}, {
					label: "Canada",
					data: [28, 48, 40, 35, 20, 33, 32],
					backgroundColor: 'rgba(24, 197, 169,.4)',
					borderColor: 'rgba(24, 197, 169,1)',
					borderWidth: 1
				}, {
					label: "United Kingdom",
					data: [4, 54, 20, 25, 32, 35, 48],
					borderColor: 'rgba(52, 153, 226,1)',
					backgroundColor: 'rgba(52, 153, 226,.4)',
					borderWidth: 1
				}],
			},
			options: {
				responsive: true,
				maintainAspectRatio: false,
				showScale: false,
				scales: {
					xAxes: [{
						display: false
					}],
					yAxes: [{
						gridLines: {
							display: false,
							drawBorder: false,
						},
					}]
				},
				legend: {
					display: false
				},
			}
		});

		visitors_data = {
			1: {
				data: [
					[20, 18, 40, 50, 35, 24, 40],
					[28, 48, 40, 35, 70, 33, 32],
					[64, 54, 60, 65, 52, 85, 48],
				],
				labels: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
			},
			2: {
				data: [
					[35, 20, 40, 24, 28, 38, 40],
					[55, 35, 32, 14, 40, 33, 22],
					[84, 62, 45, 70, 50, 85, 42],
				],
				labels: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
			},
			3: {
				data: [
					[50, 30, 25, 33, 28, 34, 45, 30, 55, 28, 42, 54],
					[45, 35, 28, 64, 40, 30, 32, 60, 50, 29, 33, 48],
					[54, 62, 35, 80, 40, 85, 42, 30, 41, 73, 68, 57],
				],
				labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
			},
		};

		$('#chart_tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			var id = $(this).attr('data-id');
			if (id && +id in visitors_data) {
				visitors_chart.data.labels = visitors_data[id].labels;
				var datasets = visitors_chart.data.datasets;
				$.each(datasets, function (index, value) {
					datasets[index].data = visitors_data[id].data[index];
				});
			}
			visitors_chart.update();
		});

	}
}
var LeafletMap = (function () {
	var map = {};
	var infoMethods = {};
	var token = 'pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpandmbXliNDBjZWd2M2x6bDk3c2ZtOTkifQ._QA7i5Mpkd_m30IGElHziw';
	var geojson;
	var map = L.map('map', {
		attributionControl: false
	}).setView([27.8, 0], 2);

	L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png?access_token=' + token + '', {
		maxZoom: 18
	}).addTo(map);

	map.doubleClickZoom.disable();
	map.scrollWheelZoom.disable();

	// --- INFO
	var info = L.control();
	info.onAdd = function (map) {
		this._div = L.DomUtil.create('div', 'info');
		this.update();
		return this._div;
	};

	info.update = function (props) {
		var visitsTopContainer,
			formattedVisitsTopContainer;

		if (props) {
			visitsTopContainer = props.visits;
			if (visitsTopContainer != 0) {
				formattedVisitsTopContainer = visitsTopContainer.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1, ");
				this._div.innerHTML = '<h4>Visits from:</h4><p><b style="color:#1f626e;font-size:15px;">' + props.name + '</b></p><p style="font-size:12px;"><b style="color:#000;font-size:16px">' + props.visitRate + '</b> traffic scale </p><p style="font-size:12px;"><b style="color:#000;font-size:16px">' + formattedVisitsTopContainer + '</b> visits</p>';
			} else {
				this._div.innerHTML = '<h4>Visits from:</h4><p><b style="color:#1f626e;font-size:15px;">' + props.name + '</b></p><p style="font-size:12px;"><b style="color:#000;font-size:16px">' + props.visitRate + '</b> traffic scale </p><p style="font-size:12px;"><b style="color:#000;font-size:16px">0</b> visits</p>';
			}
		} else {
			this._div.innerHTML = 'Hover over a state';
		}
	};

	info.addTo(map);


	// --- LEGEND
	var legend = L.control({
		position: 'bottomright'
	});

	legend.onAdd = function (map) {

		var div = L.DomUtil.create('div', 'info legend'),
			scale = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
			labels = [],
			from;
		

		for (var i = 0; i < scale.length; i++) {
			from = scale[i];

			labels.push(
				'<i style="background:' + getColor(from + 1) + '"></i>' +
				from);
		}

		div.innerHTML = labels.join('<br>');
		return div;
	};

	legend.addTo(map);

	function getColor(d) {
		return d == 0 ? '#F9FBE7' :
			d == 1 ? '#BBDEFB' :
			d == 2 ? '#90CAF9' :
			d == 3 ? '#64B5F6' :
			d == 4 ? '#42A5F5' :
			d == 5 ? '#2196F3' :
			d == 6 ? '#1E88E5' :
			d == 7 ? '#1976D2' :
			d == 8 ? '#1565C0' :
			d == 9 ? '#0D47A1' :
			'';
	}

	function style(feature) {
		return {
			weight: 1,
			opacity: .8,
			color: 'white',
			dashArray: '3',
			fillOpacity: 0.7,
			fillColor: getColor(feature.properties.visitRate)
		};
	}

	function highlightFeature(e) {
		var layer = e.target;

		layer.setStyle({
			weight: 2,
			color: '#888',
			dashArray: '',
			fillOpacity: 0.4
		});

		if (!L.Browser.ie && !L.Browser.opera) {
			layer.bringToFront();
		}

		info.update(layer.feature.properties);
	}

	function resetHighlight(e) {
		geojson.resetStyle(e.target);
		info.update();
	}

	function zoomToFeature(e) {
		map.fitBounds(e.target.getBounds());
	}

	function onEachFeature(feature, layer) {
		layer.on({
			mouseover: highlightFeature,
			mouseout: resetHighlight
		});
	}

	geojson = L.geoJson(statesData, {
		style: style,
		onEachFeature: onEachFeature
	}).addTo(map);

	map.update = function (data, visitsAll, used) {
		if (data == undefined || visitsAll == undefined || used == undefined) return false;

		geojson = L.geoJson(statesData, {
			style: style,
			onEachFeature: onEachFeature
		}).addTo(map);
	}

	return {
		map: map
	}
})();

const TableVisits = (function () {
	function TableVisits() {
		this.DOM = {
			wrapper: $('.table-states'),
			table: $('#tableStates'),
			tbody: $('#tableStates').find('tbody')
		}
		this.options = {
			"destroy": true,
			"paging": true,
			"lengthChange": false,
			"searching": true,
			"ordering": true,
			"info": false,
			"autoWidth": false,
			responsive: true
		}

		this.build()
		this.table = null;
	}

	TableVisits.prototype.build = function () {
		var self = this;
		var rows = [];

		var statesData = getData();

		if (statesData.sum("visits") === 0 && statesData.sum("visitRate") === 0) {
			buildEmptyRow()
			return false
		}

		statesData.forEach(function (state) {
			row = buildRow(state)
			rows.push(row)
		})

		this.DOM.tbody.html(rows.join(""))
		this.table = this.DOM.table.DataTable(this.options)
	}

	TableVisits.prototype.update = function () {
		var self = this;
		if (this.table !== null) {
			this.table.destroy();
		}

		self.build();
	}

	// PRIVATE METHODS
	function getData() {
		return statesJSON.get()
	}

	function buildEmptyRow() {
		var style = `border-top:1px solid #eee;
					 padding-top:2rem;
					 display:table-cell;`;

		var rowEmpty = `<tr>
							<td class="text-center" style='${style}' colspan="5">There is no data available</td>
						</tr>`;

		$('#tableStates').find('tbody').html(rowEmpty);
	}

	function buildRow(state) {
		function setFlag(id) {
			var flag = '';
			if (Number(id) > 0 && Number(id) <= 52)
				flag = 'us'
			else if (Number(id) > 52 && Number(id) <= 65)
				flag = 'ca'
			else if (Number(id) > 65 && Number(id) <= 76)
				flag = 'gb'
			else if (Number(id) > 76 && Number(id) <= 84)
				flag = 'au'
			return flag;
		}

		var flag = setFlag(state.id);
		var row = `<tr>
					<th scope="row">${state.id}</th>
					<td class="text-left">
						<span class="flag-icon flag-icon-${flag}"></span>
					</td>
					<td class="text-left">${state.name}</td>
					<td class="text-center">${state.visits}</td>
					<td class="text-center">${state.visitRate}</td>
				</tr>`;

		return row;
	}

	return TableVisits;
})();

const statesJSON = (function () {
	return {
		get: get,
		update: update,
		setToZero: setToZero
	}

	function get() {
		const states = [];
		const { features } = statesData;

		features.map((feature, index) => {
			const id = feature.id,
			name = feature.properties.name,
			visits = feature.properties.visits,
			visitRate = feature.properties.visitRate;
			
			const state = { id, name, visits, visitRate }
			states.push(state)
		})

		return states;
	}

	function update(states) {
		for (let i = 0; i < states.length; i++) {
			statesData.features[i].properties.visitRate = states[i].getAttribute('scale')
			statesData.features[i].properties.visits = states[i].getAttribute('visits')
		}
	}

	function setToZero() {
		for (let i = 0; i < statesData.features.length; i++) {
			statesData.features[i].properties.visitRate = 0
			statesData.features[i].properties.visits = 0
		}
	}
})();

const HTTPController = (function () {
	return {
		parse: parse,
		post: post
	}

	function parse(xml) {
		if (xml === undefined) return false;
		var data = {};

		var successAttribute = xml.firstChild.getAttribute("success");
		if (successAttribute === "true") {

			var responseTag = xml.getElementsByTagName("response")[0]
			var visitsAll = responseTag.getAttribute('visits')
			var used = responseTag.getAttribute('used')
			var results = xml.getElementsByTagName('results')

			data["used"] = used

			if (xml.getElementsByTagName("state")) {
				data["states"] = xml.getElementsByTagName("state")
			}

			data["criteria"] = []

			for (let i = 0; i < results.length; i++) {
				var criteriaName = results[i].getAttribute("criteria")
				var criteriaResult = results[i].getElementsByTagName('result')

				var criteriaObject = {
					name: criteriaName,
					values: [],
					proc: [],
					count: []
				}

				for (let k = 0; k < criteriaResult.length; k++) {
					criteriaObject["values"].push(criteriaResult[k].getAttribute('value'))
					criteriaObject["proc"].push(criteriaResult[k].getAttribute('proc'))
					criteriaObject["count"].push(criteriaResult[k].getAttribute('count'))
				}
				data["criteria"].push(criteriaObject)
			}
			data["visitsAll"] = visitsAll
		} else {
			var errors = xml.firstChild.firstChild.childNodes;
			errors.forEach(function (error) {
				var errorText = error.getAttribute("text")
				var errorID = error.getAttribute("id")
				$("#" + errorID).attr("title", errorText);
			})
		}

		DashboardController.update(data);
		spinner.hide();
	}

	function post(url, data, callback) {
		var req = new XMLHttpRequest()

		req.open('POST', url, true);
		req.setRequestHeader('Content-type', 'application/json');

		req.onload = function (res) {
			callback(res.target.responseXML);
		};

		req.send(JSON.stringify(data));

		setTimeout(function () {
			spinner.hide();
		}, 500)
	}

})();

var DashboardController = (function () {
	this.donut = null;

	var donut = {
		donut: null,
		init: function () {
			var self = this
			var dataValues = utils.randomArrayOfNumbers(3);
			var ctx = document.getElementById("deviceChart").getContext('2d');
			this.donut = Doughnut.init(ctx, dataValues);
		},
		update: function (data) {
			Doughnut.update(this.donut, [data.criteria[0].count[1], data.criteria[0].count[1], data.criteria[0].count[2]]);

		}
	}
	var table = {
		init: function () {
			this.tableInstance = new TableVisits();
		},

		update: function (data) {
			this.tableInstance.update()
		}
	}

	var easyPie = {
		init: function () {
			var classes = [{
					class: '.chart-1'
				},
				{
					class: '.chart-2'
				},
				{
					class: '.chart-3'
				}
			]
			var easypie = EasyPie.init(classes);
		},
		update: function (data) {
			var easyData = [{
					class: '.chart-1',
					percentage: data.criteria[0].proc[0]
				},
				{
					class: '.chart-2',
					percentage: data.criteria[0].proc[1]
				},
				{
					class: '.chart-3',
					percentage: data.criteria[0].proc[2]
				}
			];

			EasyPie.update(easyData);
			EasyPie.updateDomValues([
				{
					visits: data.visitsAll,
					criteriaValue: 'Visits',
					criteriaName: data.criteria
				},
				{
					percentage: data.criteria[0].proc[0],
					criteriaValue: data.criteria[1].values[data.criteria[1].values.length],
					criteriaName: data.criteria[0].name
				},
				{
					percentage: data.criteria[0].proc[0],
					criteriaValue: data.criteria[2].values[utils.randomNumber(data.criteria[2].values.length)],
					criteriaName: data.criteria[0].name
				}
			]);
		}
	}
	var barChart = {
		init: function () {
			var barCtx = document.getElementById("browser_chart").getContext("2d"),
				lineData = utils.randomArrayOfNumbers(6),
				labelsArray = ["2%", "1.8%", "68%", "8%", "3.34%", "0.33%"];
			var barChart = BarChart.init(barCtx, lineData, labelsArray);
		},
		update: function (data) {}
	}
	var defaultWidget = {
		init: function () {
			DefaultWidget.init();
		},
		update: function (data) {

		}
	}

	var map = {
		update: function (responseData) {
			LeafletMap.map.update(statesData, responseData.visitsAll, responseData.used);
		}
	}

	var defaultWidget1 = {
		init: function () {
			DefaultWidget1.init();
		},
		update: function (data) {

		}
	}

	var data = ''

	function initialize() {
		donut.init()
		table.init()
		easyPie.init()
		barChart.init()
		defaultWidget.init()
		defaultWidget1.init()
	}

	function update(response) {
		statesJSON.setToZero()
		statesJSON.update(response.states);
		// 0) @ Table 			
		table.update()

		// 1) @ Map 
		map.update(response)

		// 2) @ Doughnut	
		donut.update(response)

		// // 3) @ EasyPie
		easyPie.update(response)
	}

	function set() {
		$('.btn-report').on("click", function (e) {
			e.preventDefault();
			spinner.show();

			var data = {
				form: "countryid=0&propertyid=6&segmentid=0&pageid=0&datefrom=07%2F19%2F2018&dateto=07%2F19%2F2018&PerformanceReport="
			}

			HTTPController.post('/api/customerAccount', data, function (responseData) {
				HTTPController.parse(responseData)
			})
		});
	}

	// Dashboard methods 
	return {
		update: update,
		set: set,
		donut: donut,
		table: table,
		easyPie: easyPie,
		barChart: barChart,
		defaultWidget: defaultWidget,
		defaultWidget1: defaultWidget1,
		initialize: initialize,
	}


})();

const App = (function () {
	DashboardController.set();
	DashboardController.initialize();


	document.addEventListener('DOMContentLoaded', function () {
		navModule();
		criteriaBar()
		criteriaSearchWidget();
	})
})();
