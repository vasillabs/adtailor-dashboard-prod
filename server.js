const express = require('express')
const bodyParser = require('body-parser')
const path = require('path')
const xml = require('xml')
const customerAccount = require('./routes/api/customerAccount')

var app = express();

app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(bodyParser.json())
app.use(express.static(path.join(__dirname, 'public')))

app.use('/api/customerAccount', customerAccount)

const PORT = process.env.PORT || 5000;

var server = app.listen(PORT, function () {
    console.log(`The app is running on port ${PORT}`)
});