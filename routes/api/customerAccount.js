const express = require('express')
const router = express.Router()
const CustomerAccountController = require('../../controllers/customerAccountController')
const controller = new CustomerAccountController()

router.post('/', (req, res) => {
    controller.generatePerformanceReport(req, res)
})

module.exports = router