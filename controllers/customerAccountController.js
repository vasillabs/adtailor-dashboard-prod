const XMLGenerator = require('../services/generateXml')

class CustomerAccountController {
    generatePerformanceReport(req, res) {
        res.set('Content-Type', 'text/xml')    
        var xmlDoc = new XMLGenerator().build();
        res.send(xmlDoc)
    }
} 

module.exports = CustomerAccountController